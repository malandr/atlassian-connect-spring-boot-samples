package sample.connect.spring.jersey;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

@RestController
public class OAuth2DataController {

    @Autowired
    private OAuth2ClientRequestFilter oAuth2ClientRequestFilter;

    @IgnoreJwt
    @GetMapping("/oauth2-data")
    public String getOAuth2Data(@RequestParam String hostBaseUrl, @RequestParam String userAccountId) {
        oAuth2ClientRequestFilter.setUserAccountId(userAccountId);

        Client client = ClientBuilder.newClient();
        client.register(oAuth2ClientRequestFilter);
        return client.target(hostBaseUrl)
                .path("rest/api/3/myself")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String.class);
    }
}

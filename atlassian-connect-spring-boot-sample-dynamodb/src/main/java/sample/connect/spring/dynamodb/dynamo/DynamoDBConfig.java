package sample.connect.spring.dynamodb.dynamo;


import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.local.embedded.DynamoDBEmbedded;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Configuration
@EnableDynamoDBRepositories(basePackageClasses = DynamoAtlassianHostCrudRepository.class)
class DynamoDBConfig {

    private static final Logger log = LoggerFactory.getLogger(DynamoDBConfig.class);

    private static final String SQLITE_4_JAVA_LIB_PATH = "sqlite4java.library.path";

    @Bean
    AmazonDynamoDB amazonDynamoDB(@Value("${amazon.dynamodb.sqlite.file:}") String fileName) throws IOException {
        final File dbFile = dbFile(fileName);
        log.info("Using embedded DynamoDB in [{}]", dbFile != null ? dbFile : "memory");
        setSqlite4LibPath();
        return DynamoDBEmbedded.create(dbFile).amazonDynamoDB();
    }

    private File dbFile(String fileName) {
        return fileName.isEmpty() ? null : new File(fileName);
    }

    private void setSqlite4LibPath() throws IOException {
        if (System.getProperty(SQLITE_4_JAVA_LIB_PATH) == null) {
            final Path nativeLibsDirectory = nativeLibsDirectory();
            if (!Files.isDirectory(nativeLibsDirectory)) {
                throw new IllegalStateException("Unable to locate SQLite native libraries. Try running 'mvn clean install'");
            }
            System.setProperty(SQLITE_4_JAVA_LIB_PATH, nativeLibsDirectory.toString());
        }
    }

    private Path nativeLibsDirectory() throws IOException {
        return new File(".").getCanonicalFile().toPath().resolve("target").resolve("native-libs");
    }
}

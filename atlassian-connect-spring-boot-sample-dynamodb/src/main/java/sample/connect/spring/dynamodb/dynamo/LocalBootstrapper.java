package sample.connect.spring.dynamodb.dynamo;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.dynamodbv2.util.TableUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
class LocalBootstrapper implements InitializingBean {

    private static final Long READ_CAPACITY_UNITS = 100L;
    private static final Long WRITE_CAPACITY_UNITS = 100L;
    private static final ProvisionedThroughput THROUGHPUT = new ProvisionedThroughput(
            READ_CAPACITY_UNITS, WRITE_CAPACITY_UNITS);

    private final AmazonDynamoDB dynamoDB;

    LocalBootstrapper(AmazonDynamoDB dynamoDB) {
        this.dynamoDB = dynamoDB;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        final CreateTableRequest createTableReq = createTableRequest();
        TableUtils.createTableIfNotExists(dynamoDB, createTableReq);
    }

    private CreateTableRequest createTableRequest() {
        final DynamoDBMapper dynamoDBMapper = new DynamoDBMapper(dynamoDB);
        final CreateTableRequest createTableReq = dynamoDBMapper.generateCreateTableRequest(DynamoAtlassianHost.class);
        createTableReq.setProvisionedThroughput(THROUGHPUT);
        createTableReq.getGlobalSecondaryIndexes().forEach(this::configureIndex);
        return createTableReq;
    }

    private void configureIndex(GlobalSecondaryIndex index) {
        index.setProvisionedThroughput(THROUGHPUT);
        index.setProjection(new Projection().withProjectionType(ProjectionType.ALL));
    }
}

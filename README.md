# Atlassian Connect Spring Boot Samples

This repository contains sample [Spring Boot](http://projects.spring.io/spring-boot/)
applications using [`atlassian-connect-spring-boot`](https://bitbucket.org/atlassian/atlassian-connect-spring-boot), the starter for building
[Atlassian Connect](https://connect.atlassian.com/) add-ons.

## Disclaimer

Most samples use outdated dependency versions, which are likely to have known security vulnerabilities. Treat these samples as sources of inspiration; they are not ready for production.
Please upgrade to the latest versions before reporting issues with samples note working (and please consider submitting a pull request for the upgrade).

## Contents

Sample | Description
----   | -----------
[`atlassian-connect-spring-boot-sample-ajax`](atlassian-connect-spring-boot-sample-ajax) | AJAX requests to the add-on server using self-authentication tokens 
[`atlassian-connect-spring-boot-sample-atlaskit`](atlassian-connect-spring-boot-sample-atlaskit) | Using Atlaskit frontend components
[`atlassian-connect-spring-boot-sample-basic`](atlassian-connect-spring-boot-sample-basic) | Basic authentication for incoming requests
[`atlassian-connect-spring-boot-sample-cacheable-macro`](atlassian-connect-spring-boot-sample-cacheable-macro) | A Confluence Dynamic Content Macro with a cacheable iframe
[`atlassian-connect-spring-boot-sample-clojure`](atlassian-connect-spring-boot-sample-clojure) | Coding in Clojure
[`atlassian-connect-spring-boot-sample-dynamodb`](atlassian-connect-spring-boot-sample-dynamodb) | Storing AtlassianHost data in DynamoDB
[`atlassian-connect-spring-boot-sample-gradle`](atlassian-connect-spring-boot-sample-gradle) | Building using Gradle
[`atlassian-connect-spring-boot-sample-jersey-client`](atlassian-connect-spring-boot-sample-jersey-client) | Authenticated requests to Atlassian host using Jersey client
[`atlassian-connect-spring-boot-sample-retrofit`](atlassian-connect-spring-boot-sample-retrofit) | Authenticated requests to Atlassian host using Retrofit
[`atlassian-connect-spring-boot-sample-liquibase`](atlassian-connect-spring-boot-sample-liquibase) | Creating a Spring Data JPA repository with Liquibase
[`atlassian-connect-spring-boot-sample-thymeleaf`](atlassian-connect-spring-boot-sample-thymeleaf) | Rendering views using Thymeleaf

## License

This project is licensed under the [Apache License, Version 2.0](LICENSE.txt).
